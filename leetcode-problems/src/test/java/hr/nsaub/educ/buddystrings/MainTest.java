package hr.nsaub.educ.buddystrings;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class MainTest {
    private Main main = new Main();

    @Test
    public void testBuddyStrings1() {
        Assert.assertTrue(main.buddyStrings("abcd", "cbad"));
    }

    @Test
    public void testBuddyStrings1_1() {
        Assert.assertFalse(main.buddyStrings("abcde", "cbadf"));
    }

    @Test
    public void testBuddyStrings2() {
        Assert.assertFalse(main.buddyStrings("ab", "ab"));
    }

    @Test
    public void testBuddyStrings3() {
        Assert.assertTrue(main.buddyStrings("aa", "aa"));
    }

    @Test
    public void testBuddyStrings4() {
        Assert.assertTrue(main.buddyStrings("aaaaaaabc", "aaaaaaacb"));
    }

    @Test
    public void testBuddyStrings5() {
        Assert.assertFalse(main.buddyStrings("ab", "ca"));
    }

    @Test
    public void testBuddyStrings6() {
        Assert.assertTrue(main.buddyStrings("abab", "abab"));
    }

    @Test
    public void testBuddyStrings7() {
        Assert.assertTrue(main.buddyStrings("ab", "ba"));
    }

    @Test
    public void testBuddyStrings8() {
        Assert.assertFalse(main.buddyStrings("abcd", "abcd"));
    }

    @Test
    public void testBuddyStrings9() {
        Assert.assertFalse(main.buddyStrings("abac", "abad"));
    }

    @Test
    public void testBuddyStrings10() {
        Assert.assertFalse(main.buddyStrings("a", "a"));
    }
}
