package hr.nsaub.educ.canplaceflowers;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class MainTest {

    private Main main = new Main();

    @Test
    public void testCanPlaceFlowers1() {
        int[] flowers = {1, 0, 0, 0, 1};
        Assert.assertTrue(main.canPlaceFlowers(flowers, 1));
    }

    @Test
    public void testCanPlaceFlowers2() {
        int[] flowers = {1, 0, 0, 0, 1};
        Assert.assertFalse(main.canPlaceFlowers(flowers, 2));
    }

    @Test
    public void testCanPlaceFlowers3() {
        int[] flowers = {1, 0, 0, 0, 1};
        Assert.assertFalse(main.canPlaceFlowers(flowers, 2));
    }

    @Test
    public void testCanPlaceFlowers4() {
        int[] flowers = {1, 1, 1, 1, 1};
        Assert.assertTrue(main.canPlaceFlowers(flowers, 0));
    }

    @Test
    public void testCanPlaceFlowers5() {
        int[] flowers = {0, 0, 0, 0, 1};
        Assert.assertTrue(main.canPlaceFlowers(flowers, 2));
    }

    @Test
    public void testCanPlaceFlowers6() {
        int[] flowers = {0};
        Assert.assertTrue(main.canPlaceFlowers(flowers, 1));
    }

    @Test
    public void testCanPlaceFlowers7() {
        int[] flowers = {1};
        Assert.assertFalse(main.canPlaceFlowers(flowers, 1));
    }

    @Test
    public void testCanPlaceFlowers8() {
        int[] flowers = {0, 1, 0};
        Assert.assertFalse(main.canPlaceFlowers(flowers, 1));
    }

    @Test
    public void testCanPlaceFlowers9() {
        int[] flowers = {0, 0, 1, 0, 0};
        Assert.assertTrue(main.canPlaceFlowers(flowers, 1));
    }
}
