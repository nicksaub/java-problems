package hr.nsaub.educ.thirdmaxnumber;

import hr.nsaub.educ.thirdmaxnumber.Main;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class MainTest {
    private Main main = new Main();

    @Test
    public void testThirdMax1() {
        int[] nums = {1,2,3};
        Assert.assertTrue(main.thirdMax(nums) == 1);
    }

    @Test
    public void testThirdMax2() {
        int[] nums = {1,2};
        Assert.assertTrue(main.thirdMax(nums) == 2);
    }

    @Test
    public void testThirdMax3() {
        int[] nums = {2,2,3,1};
        Assert.assertTrue(main.thirdMax(nums) == 1);
    }
}
