package hr.nsaub.educ.validmountain;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class MainTest {
    private Main main = new Main();

    @Test
    public void testValidMountainArray1() {
        int[] arr = {0,3,2,1};
        Assert.assertTrue(main.validMountainArray(arr));
    }

    @Test
    public void testValidMountainArray2() {
        int[] arr = {2,1};
        Assert.assertFalse(main.validMountainArray(arr));
    }

    @Test
    public void testValidMountainArray3() {
        int[] arr = {3,5,5};
        Assert.assertFalse(main.validMountainArray(arr));
    }

    @Test
    public void testValidMountainArray4() {
        int[] arr = {0,2,3,4,5,2,1,0};
        Assert.assertTrue(main.validMountainArray(arr));
    }

    @Test
    public void testValidMountainArray5() {
        int[] arr = {0,1,2,3,4,5,6,7,8,9};
        Assert.assertFalse(main.validMountainArray(arr));
    }

    @Test
    public void testValidMountainArray6() {
        int[] arr = {4,3,2,1};
        Assert.assertFalse(main.validMountainArray(arr));
    }
}
