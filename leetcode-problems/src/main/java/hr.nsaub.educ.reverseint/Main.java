package hr.nsaub.educ.reverseint;

import java.util.ArrayList;
import java.util.List;

/**
 * Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside
 * the signed 32-bit integer range [-231, 231 - 1], then return 0.
 */
public class Main {

    public int reverse(int x) {
        int retval = 0;
        int sign = x >= 0 ? 1 : -1;

        if(x > ((int)Math.pow(2, 31) - 1) || x < -1*(int)Math.pow(2, 31)) {
            retval = 0;
        }
        else {
            List<Integer> diggits = new ArrayList<Integer>();

            x = Math.abs(x);
            do {
                diggits.add(x % 10);
                x = x / 10;
            } while (x != 0);

            int pot = diggits.size() - 1;

            try {
                for(Integer diggit : diggits) {
                    retval = Math.addExact(retval, Math.multiplyExact(diggit ,(int)Math.pow(10, pot)));
                    pot--;
                }
            } catch (ArithmeticException e) {
                retval = 0;
            }
        }

        return sign * retval;
    }
}
