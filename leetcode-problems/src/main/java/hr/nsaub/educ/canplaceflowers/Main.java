package hr.nsaub.educ.canplaceflowers;

/**
 * You have a long flowerbed in which some of the plots are planted, and some are not.
 * However, flowers cannot be planted in adjacent plots.
 * <p>
 * Given an integer array flowerbed containing 0's and 1's, where 0 means empty and 1 means not empty, and an integer n,
 * return if n new flowers can be planted in the flowerbed without violating the no-adjacent-flowers rule.
 */
public class Main {

    public boolean canPlaceFlowers(int[] flowerbed, int n) {
        int newFlowers = 0;

        if (flowerbed.length >= 1 && flowerbed.length <= 2 * Math.pow(10, 4) && n >= 0 && n <= flowerbed.length) {
            for (int i = 0; i < flowerbed.length; i++) {
                if (flowerbed[i] != 0 && flowerbed[i] != 1) {
                    throw new IllegalArgumentException();
                } else if (flowerbed.length == 1 && flowerbed[0] == 0) {
                    newFlowers++;
                } else if (flowerbed.length == 1 && flowerbed[0] == 1) {
                    continue;
                } else if (i == 0 && flowerbed[0] == 0 && flowerbed[1] == 0) {
                    newFlowers++;
                    flowerbed[0] = 1;
                } else if (i == 0 && (flowerbed[0] == 1 || (flowerbed[0] == 0 && flowerbed[1] == 1))) {
                    continue;
                } else if (i == flowerbed.length - 1 && flowerbed[flowerbed.length - 2] == 0 &&
                    flowerbed[flowerbed.length - 1] == 0) {
                    newFlowers++;
                } else if (flowerbed[i] == 0 && flowerbed[i - 1] == 0 && flowerbed[i + 1] == 0) {
                    newFlowers++;
                    flowerbed[i] = 1;
                }
            }
        } else {
            throw new IllegalArgumentException();
        }

        return n <= newFlowers;
    }

}
