package hr.nsaub.educ.validmountain;

/**
 * Given an array of integers arr, return true if and only if it is a valid mountain array.
 *
 * Recall that arr is a mountain array if and only if:
 *
 *  - arr.length >= 3
 *  - There exists some i with 0 < i < arr.length - 1 such that:
 *    arr[0] < arr[1] < ... < arr[i - 1] < arr[i]
 *    arr[i] > arr[i + 1] > ... > arr[arr.length - 1]
 */
public class Main {

    public boolean validMountainArray(int[] arr) {
        if(arr.length >= 3 && arr.length <= Math.pow(10, 4)) {
            boolean retval = true;
            boolean ascending = true;

            for(int i = 0; i <arr.length; i++) {
                if(i == 1 && arr[i] < arr[i-1]) {
                    retval = false;
                    break;
                }
                else if(i > 0 && ascending && arr[i] < arr[i-1]) {
                    ascending = false;
                }
                else if(i > 0 && !ascending && arr[i] > arr[i-1]) {
                    retval = false;
                    break;
                }
                else if(i > 0 && arr[i] == arr[i-1]) {
                    retval = false;
                    break;
                }
            }

            if(ascending) {
                retval = false;
            }

            return retval;
        }
        else {
            return false;
        }
    }
}
