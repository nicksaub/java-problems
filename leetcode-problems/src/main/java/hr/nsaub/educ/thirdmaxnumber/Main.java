package hr.nsaub.educ.thirdmaxnumber;

/**
 *
 * Given integer array nums, return the third maximum number in this array.
 * If the third maximum does not exist, return the maximum number.
 *
 */
public class Main {

    public int thirdMax(int[] nums) {
        Integer max = null;
        Integer secondMax = null;
        Integer thirdMax = null;

        if(nums.length >= 1 && nums.length <= Math.pow(10, 4)) {
            for (int num : nums) {
                if (max == null || num > max) {
                    thirdMax = secondMax;
                    secondMax = max;
                    max = num;
                } else if ((num < max  && secondMax == null) || (num < max && num > secondMax)) {
                    thirdMax = secondMax;
                    secondMax = num;
                } else if ((secondMax != null && num < secondMax && thirdMax == null) ||
                    (secondMax != null && thirdMax != null && num < secondMax && num > thirdMax)) {
                    thirdMax = num;
                }
            }

            return thirdMax != null ? thirdMax : max;
        }
        else {
            throw new IllegalArgumentException();
        }
    }
}
