package hr.nsaub.educ.buddystrings;

import java.util.HashSet;
import java.util.Set;

/**
 * Given two strings a and b, return true if you can swap two letters in a so the result is equal to b,
 * otherwise, return false.
 * Swapping letters is defined as taking two indices i and j (0-indexed) such that i != j and swapping the characters
 * at a[i] and b[j]. For example, swapping at indices 0 and 2 in "abcd" results in "cbad".
 */
public class Main {

    public boolean buddyStrings(String a, String b) {
        boolean retval = true;

        if(a == null || b == null || a.length() != b.length() ||
            !(a.length() >= 1 && a.length() <= 2*(int)Math.pow(10, 4))) {
            retval = false;
        }
        else if (a.length() == 1) {
            retval = false;
        }
        else {
            int foundDiffs = 0;
            Character charInA = null;
            Character charInB = null;
            boolean isPalindrome = true;
            boolean allDistinct = true;
            Set<Character> charSet = new HashSet<>();
            for(int i = 0; i < a.length(); i++) {
                if(a.charAt(i) != b.charAt(i)) {
                    if(foundDiffs == 0) {
                        foundDiffs++;
                        charInA = a.charAt(i);
                        charInB = b.charAt(i);
                    }
                    else if(foundDiffs == 1) {
                        if(!charInA.equals(b.charAt(i)) || !charInB.equals(a.charAt(i))) {
                            retval = false;
                            break;
                        }
                        else {
                            foundDiffs++;
                        }
                    }
                    else if(foundDiffs > 1) {
                        foundDiffs++;
                        retval = false;
                        break;
                    }
                }
                if(isPalindrome && a.charAt(i) != b.charAt(a.length()-1-i)) {
                    isPalindrome = false;
                }
                if(allDistinct && !charSet.contains(a.charAt(i))) {
                    charSet.add(a.charAt(i));
                }
                else {
                    allDistinct = false;
                }
            }

            if((foundDiffs == 0 && !isPalindrome && allDistinct) ||
                (foundDiffs != 0 && foundDiffs != 2 && !isPalindrome)) {
                retval = false;
            }
        }


        return retval;
    }
}
