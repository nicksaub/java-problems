package hr.nsaub.educ.reader;

import hr.nsaub.educ.model.TestCase;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TestCaseReader {
    private File file;
    private List<TestCase> testCases = new ArrayList<TestCase>();

    public TestCaseReader(File file) {
        this.file = file;
    }

    /**
     * Reads variable lists and constraints from a text file crating test cases.
     * @throws IOException
     */
    public void readTestCases() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line = bufferedReader.readLine();
        TestCase testCase = new TestCase();

        while(line != null){
            testCase.setVariables(new HashSet<String>(Arrays.asList(line.split(" "))));

            line = bufferedReader.readLine();
            String[] constraintValues = line.split(" ");

            Boolean newConstraint = true;
            String currentConstraint = null;
            for(String constraintValue : constraintValues){
                if(newConstraint){
                    currentConstraint = constraintValue;
                    newConstraint = false;
                }
                else{
                    if(testCase.getConstraints().containsKey(currentConstraint)){
                        testCase.getConstraints().get(currentConstraint).add(constraintValue);
                    }
                    else{
                        Set<String> constraints = new HashSet<String>();
                        constraints.add(constraintValue);
                        testCase.getConstraints().put(currentConstraint, constraints);
                    }
                    newConstraint = true;
                }
            }

            testCases.add(testCase);
            line = bufferedReader.readLine();
            testCase = new TestCase();
        }
    }

    public List<TestCase> getTestCases() {
        return testCases;
    }
}
