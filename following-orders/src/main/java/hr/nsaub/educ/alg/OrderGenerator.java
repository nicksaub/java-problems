package hr.nsaub.educ.alg;

import java.util.*;

/**
 * Class that implments the algorithm for order generation based on the given variables and constraints.
 */
public class OrderGenerator {
    private Set<String> variables;
    private Map<String, Set<String>> constraints;

    public OrderGenerator(Set<String> variables,
                          Map<String, Set<String>> constraints) {
        this.variables = variables;
        this.constraints = constraints;
    }

    public Set<String> generateOrders() {
        Set<String> orders = new HashSet<String>();

        for (String variable : variables) {
            for (int pos = 0; pos < variables.size(); pos++) {
                String[] order = new String[variables.size()];
                order[pos] = variable;
            }
        }

        return orders;
    }
}
