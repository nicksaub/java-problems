package hr.nsaub.educ.model;

import hr.nsaub.educ.alg.OrderGenerator;

import java.util.*;

/**
 * A class that represents a single test case i.e. a set of variables and a set of contraints for which all possible
 * orders are to be generated.
 */
public class TestCase {
    private Set<String> variables = new HashSet<String>();
    private Map<String, Set<String>> constraints = new HashMap<String, Set<String>>();

    public Set<String> getVariables() {
        return variables;
    }

    public void setVariables(Set<String> variables) {
        this.variables = variables;
    }

    public Map<String, Set<String>> getConstraints() {
        return constraints;
    }

    public void setConstraints(Map<String, Set<String>> constraints) {
        this.constraints = constraints;
    }

    /**
     * Returns a list of all possible orders from the generator.
     * @return
     */
    public List<String> getResult(){
        OrderGenerator orderGenerator = new OrderGenerator(variables, constraints);
        return orderGenerator.generateOrders();
    }
}
