package hr.nsaub.educ.alg;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;

@RunWith(JUnit4.class)
public class OrderGeneratorTest {
    private Set<String> variables;
    private Map<String, Set<String>> contraints;

    @Test
    public void testGenerateOrders1() {
        variables = new HashSet<String>(Arrays.asList("a", "b", "f", "g"));
        contraints = new HashMap<String, Set<String>>();
        contraints.put("a", new HashSet<String>(Arrays.asList("b")));
        contraints.put("b", new HashSet<String>(Arrays.asList("f")));

        OrderGenerator orderGenerator = new OrderGenerator(variables, contraints);
        Set<String> orders = orderGenerator.generateOrders();

        Assert.assertTrue(orders.contains("abfg"));
        Assert.assertTrue(orders.contains("abgf"));
        Assert.assertTrue(orders.contains("agbf"));
        Assert.assertTrue(orders.contains("gabf"));
    }

    @Test
    public void testGenerateOrders2() {
        variables = new HashSet<String>(Arrays.asList("v", "w", "x", "y", "z"));
        contraints = new HashMap<String, Set<String>>();
        contraints.put("v", new HashSet<String>(Arrays.asList("y")));
        contraints.put("x", new HashSet<String>(Arrays.asList("v")));
        contraints.put("z", new HashSet<String>(Arrays.asList("v")));
        contraints.put("w", new HashSet<String>(Arrays.asList("v")));

        OrderGenerator orderGenerator = new OrderGenerator(variables, contraints);
        Set<String> orders = orderGenerator.generateOrders();

        Assert.assertTrue(orders.contains("wxzvy"));
        Assert.assertTrue(orders.contains("wzxvy"));
        Assert.assertTrue(orders.contains("xwzvy"));
        Assert.assertTrue(orders.contains("xzwvy"));
    }
}
