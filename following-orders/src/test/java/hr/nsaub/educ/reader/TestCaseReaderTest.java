package hr.nsaub.educ.reader;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

@RunWith(JUnit4.class)
public class TestCaseReaderTest {

    @Test
    public void testReadTestCases() throws Exception{
        TestCaseReader testCaseReader = new TestCaseReader(new File("testCases.txt"));
        testCaseReader.readTestCases();

        Assert.assertEquals(2, testCaseReader.getTestCases().size());
        Assert.assertEquals(4, testCaseReader.getTestCases().get(0).getVariables().size());
        Assert.assertEquals(2, testCaseReader.getTestCases().get(0).getConstraints().values().size());
        Assert.assertTrue(testCaseReader.getTestCases().get(0).getConstraints().get("a").contains("b"));
        Assert.assertTrue(testCaseReader.getTestCases().get(0).getConstraints().get("b").contains("f"));
        Assert.assertEquals(5, testCaseReader.getTestCases().get(1).getVariables().size());
        Assert.assertEquals(4, testCaseReader.getTestCases().get(1).getConstraints().values().size());
        Assert.assertTrue(testCaseReader.getTestCases().get(1).getConstraints().get("v").contains("y"));
        Assert.assertTrue(testCaseReader.getTestCases().get(1).getConstraints().get("x").contains("v"));
        Assert.assertTrue(testCaseReader.getTestCases().get(1).getConstraints().get("z").contains("v"));
        Assert.assertTrue(testCaseReader.getTestCases().get(1).getConstraints().get("w").contains("v"));
    }

}
