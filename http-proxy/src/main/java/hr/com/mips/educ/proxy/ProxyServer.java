package hr.com.mips.educ.proxy;

import sun.net.www.protocol.https.HttpsURLConnectionImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;

public class ProxyServer {
    private Integer port;

    public ProxyServer(Integer port) {
        this.port = port;
    }

    public void openSocket() throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);

        while (true) {
            final Socket clientSocket = serverSocket.accept();

            new Thread() {
                public void run() {
                    BufferedReader bufferedReader = null;
                    PrintWriter printWriter = null;
                    HttpURLConnection httpURLConnection = null;

                    try {
                        bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                        String line = null;
                        Boolean first = true;
                        String method = "GET";
                        String urlStr = "http://www.google.com";
                        while (bufferedReader.ready()) {
                            line = bufferedReader.readLine();

                            if(first){
                                String[] tokens = line.split(" ");
                                method = tokens[0];
                                urlStr = tokens[1];
                                first = false;
                            }

                            if(!urlStr.equals("http://detectportal.firefox.com/success.txt")) {
                                System.out.println(line);
                            }
                        }

                        printWriter = new PrintWriter(clientSocket.getOutputStream());

                        URL url = new URL(urlStr);
                        httpURLConnection = (HttpURLConnection) url.openConnection();
                        httpURLConnection.setRequestMethod(method);
                        int responseCode = httpURLConnection.getResponseCode();
                        if(!urlStr.equals("http://detectportal.firefox.com/success.txt")) {
                            System.out.println("Response status: " + responseCode);
                        }

                        BufferedReader in = new BufferedReader(
                            new InputStreamReader(httpURLConnection.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();

                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }
                        in.close();

                        printWriter.println("HTTP/1.1 200 OK\r\n\r\n" + response.toString());
                        printWriter.flush();
                        clientSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally{
                        try {
                            if(bufferedReader != null){
                                bufferedReader.close();
                            }
                            if(printWriter != null){
                                printWriter.close();;
                            }
                            if(httpURLConnection != null){
                                httpURLConnection.disconnect();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        }
    }

    public static void main(String[] args) {
        try {
            ProxyServer proxyServer = new ProxyServer(4444);
            proxyServer.openSocket();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
