package hr.com.mips.educ.proxy;

import java.io.*;
import java.net.Socket;

public class ProxyClient {

    public static void main(String[] args){
        try {
            Socket socket = new Socket("localhost", 4444);

            OutputStream output = socket.getOutputStream();
            PrintWriter writer = new PrintWriter(output, true);
            writer.println("GET https://www.index.hr/ HTTP/1.1");
            writer.println("Host: www.index.hr");
            writer.flush();

            InputStream input = socket.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));

            String response = null;
            while((response = reader.readLine()) != null) {
                System.out.println(response);
            }
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
