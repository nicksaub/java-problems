package hr.nsaub.educ.alg;

import hr.nsaub.educ.model.BinaryTree;

/**
 * Class that implements the alogrithm for calculating root to leaf sums.
 */
public class TreeSumChecker {
    private BinaryTree root;
    private int sum;

    public TreeSumChecker(BinaryTree root, int sum) {
        this.root = root;
        this.sum = sum;
    }

    public boolean sumExists() {
        boolean retval = false;

        if (root.getLeft() != null && sumExists(root.getLeft(), root.getValue())) {
            retval = true;
        } else if (root.getRight() != null && sumExists(root.getRight(), root.getValue())) {
            retval = true;
        } else if (root.getValue() != null && root.getValue().equals(sum)) {
            retval = true;
        }

        return retval;
    }

    /**
     * A recurisive method that walks from the root to all possible leafs and calculates the sums.
     * @param node
     * @param currentSum
     * @return
     */
    private boolean sumExists(BinaryTree node, int currentSum) {
        boolean retval = false;
        currentSum += node.getValue();

        if (node.getLeft() == null && node.getRight() == null) {
            if (currentSum == this.sum) {
                retval = true;
            }
        } else if (currentSum < this.sum && node.getLeft() != null && sumExists(node.getLeft(), currentSum)) {
            retval = true;
        } else if (currentSum < this.sum && node.getRight() != null && sumExists(node.getRight(), currentSum)) {
            retval = true;
        }

        return retval;
    }
}
