package hr.nsaub.educ.reader;

import hr.nsaub.educ.model.TestCase;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class TestCaseReader {
    private File file;
    private List<TestCase> testCases = new ArrayList<TestCase>();

    public TestCaseReader(File file) {
        this.file = file;
    }

    /**
     * Reads integer/tree expression pairs from the input file taking care that a single test case can be spread across
     * multiple lines.
     * @throws IOException
     */
    public void readTestCases() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line = bufferedReader.readLine();
        Boolean newTestCase = true;
        TestCase testCase = new TestCase();

        while(line != null){
            if(newTestCase){
                String sum = line.split("\\s+")[0];
                testCase.setSum(Integer.parseInt(sum));
                testCase.setBinaryTreeExpression(new StringBuilder(line.substring(sum.length()).replace(" ", "")));

                if(occurenceOf(testCase.getBinaryTreeExpression().toString(), ')') !=
                    occurenceOf(testCase.getBinaryTreeExpression().toString(), '(')){
                    newTestCase = false;
                }
                else{
                    testCase.buildTreeModel();
                    this.testCases.add(testCase);
                    testCase = new TestCase();
                }
            }
            else{
                testCase.getBinaryTreeExpression().append(line.replace(" ", ""));

                if(occurenceOf(testCase.getBinaryTreeExpression().toString(), ')') ==
                    occurenceOf(testCase.getBinaryTreeExpression().toString(), '(')){
                    testCase.buildTreeModel();
                    this.testCases.add(testCase);
                    newTestCase = true;
                    testCase = new TestCase();
                }
            }

            line = bufferedReader.readLine();
        }

        bufferedReader.close();
    }

    /**
     * Counts occurences of the given character in the given string.
     * @param string
     * @param character
     * @return
     */
    private int occurenceOf(String string, char character){
        int retval=0;

        for(int i=0; i<string.length(); i++){
            if(string.charAt(i) == character) retval++;
        }

        return retval;
    }

    public List<TestCase> getTestCases() {
        return testCases;
    }
}
