package hr.nsaub.educ.model;

import hr.nsaub.educ.alg.TreeSumChecker;

import java.util.Scanner;

/**
 * A class that represents a single test case i.e. am integer/tree pair for which the check is performed.
 */
public class TestCase {
    private Integer sum;
    private BinaryTree binaryTree;
    private StringBuilder binaryTreeExpression;

    public TestCase() {
    }

    public TestCase(Integer sum, StringBuilder binaryTreeExpression) {
        this.sum = sum;
        this.binaryTreeExpression = binaryTreeExpression;
    }

    /**
     * Creates a binary tree model from the existing tree expression.
     */
    public void buildTreeModel() {
        if (binaryTreeExpression == null || binaryTreeExpression.length() == 0)
            throw new IllegalStateException("Binary tree expression not defined!");
        String treeExpression = binaryTreeExpression.toString();

        if (treeExpression.length() == 2) {
            binaryTree = new BinaryTree();
        } else {
            binaryTree = buildTreeModel(binaryTreeExpression.substring(1, binaryTreeExpression.length() - 1));
        }
    }

    /**
     * A recursive function that work from the outside in creating the binary tree from the expression.
     * @param subExpression
     * @return
     */
    private BinaryTree buildTreeModel(String subExpression) {
        if(!subExpression.equals("")){
            Scanner scanner = new Scanner(subExpression);
            scanner.useDelimiter("");
            int index = 0;

            StringBuilder value = new StringBuilder();
            while (!scanner.hasNext("\\(")) {
                value.append(scanner.next());
                index++;
            }
            scanner.next();
            index++;
            int openCnt = 1;
            int closedCnt = 0;

            while (openCnt > closedCnt) {
                String token = scanner.next();
                index++;
                if (token.equals("(")) {
                    openCnt++;
                } else if (token.equals(")")) {
                    closedCnt++;
                }
            }

            return new BinaryTree(Integer.parseInt(value.toString()),
                buildTreeModel(subExpression.substring(value.length()+1, index - 1)),
                buildTreeModel(subExpression.substring(index + 1, subExpression.length() - 1)));
        }
        else {
            return null;
        }
    }

    /**
     * Checks if the given binary tree has a root to leaf path with the given sum.
     *
     * @return
     */
    public String getResult() {
        if (sum == null)
            throw new IllegalStateException("Sum not defined!");
        if (binaryTree == null)
            throw new IllegalStateException("Tree not built!");

        String result;
        TreeSumChecker treeSumChecker = new TreeSumChecker(binaryTree, sum);

        if (treeSumChecker.sumExists()) {
            result = "Yes";
        } else {
            result = "No";
        }

        return result;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public StringBuilder getBinaryTreeExpression() {
        return binaryTreeExpression;
    }

    public void setBinaryTreeExpression(StringBuilder binaryTreeExpression) {
        this.binaryTreeExpression = binaryTreeExpression;
    }
}
