package hr.nsaub.educ.model;

/**
 * Binary tree model.
 */
public class BinaryTree {
    /**
     * Value of the current node.
     */
    private Integer value;
    /**
     * Left subtree.
     */
    private BinaryTree left;
    /**
     * Right subtree.
     */
    private BinaryTree right;

    public BinaryTree() {
    }

    public BinaryTree(Integer value, BinaryTree left, BinaryTree right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public Integer getValue() {
        return value;
    }

    public BinaryTree getLeft() {
        return left;
    }

    public BinaryTree getRight() {
        return right;
    }
}
