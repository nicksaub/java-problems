package hr.nsaub.educ;

import hr.nsaub.educ.model.TestCase;
import hr.nsaub.educ.reader.TestCaseReader;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        if(args.length != 1){
            throw new IllegalArgumentException("Illegal number of arguments! Expected one.");
        }

        TestCaseReader testCaseReader = new TestCaseReader(new File(args[0]));
        try {
            testCaseReader.readTestCases();
            List<TestCase> testCases = testCaseReader.getTestCases();

            for(TestCase testCase : testCases){
                System.out.println(testCase.getResult());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
