package hr.nsaub.educ.reader;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;

@RunWith(JUnit4.class)
public class TestCaseReaderTest {

    @Test
    public void testReadTestCases() throws Exception{
        TestCaseReader testCaseReader = new TestCaseReader(new File("testCases.txt"));
        testCaseReader.readTestCases();

        Assert.assertEquals(4, testCaseReader.getTestCases().size());
    }
}
