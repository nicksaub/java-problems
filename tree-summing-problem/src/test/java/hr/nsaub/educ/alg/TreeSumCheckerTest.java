package hr.nsaub.educ.alg;

import hr.nsaub.educ.model.BinaryTree;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TreeSumCheckerTest {
    private BinaryTree binaryTree;

    @Before
    public void before(){
        binaryTree = new BinaryTree(10,
            new BinaryTree(2,
                new BinaryTree(6, null, null),
                null),
            new BinaryTree(5,
                new BinaryTree(4,
                    null,
                    new BinaryTree(1, null, null)),
                new BinaryTree(8,
                    null,
                    new BinaryTree(3, null, null))));
    }

    @Test
    public void testSumExists_empty_tree(){
        TreeSumChecker treeSumChecker = new TreeSumChecker(new BinaryTree(), 5);

        Assert.assertFalse(treeSumChecker.sumExists());
    }

    @Test
    public void testSumExists_exists_depth_0(){
        BinaryTree binaryTree = new BinaryTree(10, null, null);
        TreeSumChecker treeSumChecker = new TreeSumChecker(binaryTree, 10);

        Assert.assertTrue(treeSumChecker.sumExists());
    }

    @Test
    public void testSumExists_doesnt_exist_depth_0(){
        BinaryTree binaryTree = new BinaryTree(10, null, null);
        TreeSumChecker treeSumChecker = new TreeSumChecker(binaryTree, 7);

        Assert.assertFalse(treeSumChecker.sumExists());
    }

    @Test
    public void testSumExists_exists_1(){
        TreeSumChecker treeSumChecker = new TreeSumChecker(binaryTree, 18);

        Assert.assertTrue(treeSumChecker.sumExists());
    }

    @Test
    public void testSumExists_exists_2(){
        TreeSumChecker treeSumChecker = new TreeSumChecker(binaryTree, 20);

        Assert.assertTrue(treeSumChecker.sumExists());
    }

    @Test
    public void testSumExists_exists_3(){
        TreeSumChecker treeSumChecker = new TreeSumChecker(binaryTree, 26);

        Assert.assertTrue(treeSumChecker.sumExists());
    }

    @Test
    public void testSumExists_doesnt_exist_1(){
        TreeSumChecker treeSumChecker = new TreeSumChecker(binaryTree, 14);

        Assert.assertFalse(treeSumChecker.sumExists());
    }
}
