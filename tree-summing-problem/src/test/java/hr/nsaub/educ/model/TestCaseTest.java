package hr.nsaub.educ.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TestCaseTest {
    private TestCase testCase;

    @Test
    public void testBuildTreeModel1() {
        testCase = new TestCase(27,
            new StringBuilder("(5(4(11(7()())(2()()))())(8(13()())(4()(1()()))))"));
        testCase.buildTreeModel();

        Assert.assertEquals("Yes", testCase.getResult());
    }

    @Test
    public void testBuildTreeModel2() {
        testCase = new TestCase(26,
            new StringBuilder("(5(4(11(7()())(2()()))())(8(13()())(4()(1()()))))"));
        testCase.buildTreeModel();

        Assert.assertEquals("Yes", testCase.getResult());
    }

    @Test
    public void testBuildTreeModel3() {
        testCase = new TestCase(18,
            new StringBuilder("(5(4(11(7()())(2()()))())(8(13()())(4()(1()()))))"));
        testCase.buildTreeModel();

        Assert.assertEquals("Yes", testCase.getResult());
    }

    @Test
    public void testBuildTreeModel4() {
        testCase = new TestCase(22,
            new StringBuilder("(5(4(11(7()())(2()()))())(8(13()())(4()(1()()))))"));
        testCase.buildTreeModel();

        Assert.assertEquals("Yes", testCase.getResult());
    }

    @Test
    public void testBuildTreeModel5() {
        testCase = new TestCase(45,
            new StringBuilder("(5(4(11(7()())(2()()))())(8(13()())(4()(1()()))))"));
        testCase.buildTreeModel();

        Assert.assertEquals("No", testCase.getResult());
    }

    @Test
    public void testBuildTreeModel6() {
        testCase = new TestCase(11,
            new StringBuilder("(5(4(11(7()())(2()()))())(8(13()())(4()(1()()))))"));
        testCase.buildTreeModel();

        Assert.assertEquals("No", testCase.getResult());
    }
}
